import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'we-always-learn-app';
  navigation = [
    { link: 'about', label: 'About' },
    { link: 'feature-list', label: 'Features' },
    { link: 'weather', label: 'Weather' },
  ];
}
