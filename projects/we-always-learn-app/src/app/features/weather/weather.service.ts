import { WeatherNewsModel } from './weather.model';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  url = 'https://data.tmd.go.th/api/WeatherWarningNews/v1/?uid=u62prasom.t&ukey=38c21f9ba360d305eaca4ff2a792509a&format=json';
  todayUrl = 'https://data.tmd.go.th/api/WeatherToday/V2/?uid=u62prasom.&ukey=38c21f9ba360d305eaca4ff2a792509a&format=json'
  constructor(
    private http: HttpClient
  ) { }

  getWeatherNews(): Observable<WeatherNewsModel> {
    return this.http.get<WeatherNewsModel>(this.url);
    // return of(mock);
  }

  getWeatherToday(){

  }
}

const mock: WeatherNewsModel = {
  "@attributes": {
    "version": "1.0"
  },
  "header": {
    "title": "Thailand Weather and Climate  News",
    "description": "Weather warning news, climate news and weather tracking news ",
    "uri": "http://data.tmd.go.th/api/WeatherWarningNews/v1/index.php",
    "lastBuildDate": "2019-11-06 13:53:42",
    "copyRight": "Thai Meteorological Department:2558",
    "generator": "TMDData_API Services",
    "status": "200 OK"
  },
  "WarningNews": {
    "IssueNo": "6",
    "AnnounceDateTime": "2019-11-06 12:03:36.000",
    "TitleThai": "พายุระดับ 3 (โซนร้อน) ෳนากรี෴ (มีผลกระทบในช่วงวันที่ 10-11 พฤศจิกายน 2562)",
    "TitleEnglish": "Tropical Storm ෳNAKRI෴ category 3",
    "DescriptionThai": "เมื่อเวลา 10.00 น.ของวันนี้ (6 พ.ย. 2562) พายุระดับ 3 (โซนร้อน) ෳนากรี෴ มีศูนย์กลางอยู่ที่ละติจูด 13.6 องศาเหนือ ลองจิจูด 116.2 องศาตะวันออก พายุนี้เกือบไม่เคลื่อนที่ ความเร็วลมสูงสุดใกล้จุดศูนย์กลางประมาณ 75 กิโลเมตรต่อชั่วโมง มีแนวโน้มเคลื่อนตัวทางทิศตะวันตก คาดว่าจะเคลื่อนเข้าใกล้ชายฝั่งประเทศเวียดนามตอนกลางในช่วงวันที่ 10-11 พ.ย. 2562 ขอให้ผู้ที่จะเดินทางไปประเทศเวียดนามตรวจสอบสภาพอากาศก่อนออกเดินทางไว้ด้วย พายุนี้ยังไม่ส่งผลกระทบต่อลักษณะอากาศของประเทศไทยในระยะนี้ จึงขอให้ประชาชนติดตามประกาศกรมอุตุนิยมวิทยาอย่างใกล้ชิดต่อไป\r\nประชาชนสามารถติดตามข้อมูลที่เว็บไซต์กรมอุตุนิยมวิทยา http://www.tmd.go.th หรือ สายด่วนพยากรณ์อากาศ 1182 ได้ตลอด 24 ชั่วโมง\r\nประกาศ  ณ วันที่ 6 พฤศจิกายน พ.ศ. 2562 เวลา 11.00 น.\r\nกรมอุตุนิยมวิทยาจะออกประกาศฉบับต่อไป ในวันที่ 6 พฤศจิกายน พ.ศ. 2562 เวลา 23.00 น.\r\n\r\n\r\n (ลงชื่อ)         ภูเวียง  ประคำมินทร์    \r\n                         (นายภูเวียง  ประคำมินทร์)\r\n\t              อธิบดีกรมอุตุนิยมวิทยา\r\n",
    "DocumentFile": "http://www.tmd.go.th/programs\\uploads\\announces\\2019-11-06_12033.pdf",
    "WarningTypeThai": "ข่าวเตือนภัย",
    "WarningTypeEnglish": "Warning news",
    "ContactDetail": {}
  }
};
