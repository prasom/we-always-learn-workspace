import { WeatherNewsModel, WarningNew } from './weather.model';

import { WeatherActions, WeatherActionTypes } from './weather.actions';

export const weatherFeatureKey = 'weather';

export interface State {
  news: WarningNew;
  newsLoading: boolean;
}

export const initialState: State = {
  news: null,
  newsLoading: false,
};

export function reducer(state = initialState, action: WeatherActions): State {
  switch (action.type) {

    case WeatherActionTypes.LoadWeathers:
      return { ...state, newsLoading: true };
    case WeatherActionTypes.LoadWeatherSuccess:
      return {
        ...state,
        news: action.payload,
        newsLoading: false
      };
    case WeatherActionTypes.LoadWeatherFail:
      return { ...state, news: action.payload, newsLoading: false };
    default:
      return state;
  }
}
