import { Action } from '@ngrx/store';
import { WeatherNewsModel, WarningNew } from './weather.model';

export enum WeatherActionTypes {
  LoadWeathers = '[Weather] Load Weathers',
  LoadWeatherSuccess = '[Weather] Load Weathers Success',
  LoadWeatherFail = '[Weather] Load Weathers Fail',


}

export class LoadWeathers implements Action {
  readonly type = WeatherActionTypes.LoadWeathers;
}

export class LoadWeatherSuccess implements Action {
  readonly type = WeatherActionTypes.LoadWeatherSuccess;
  constructor(public payload: WarningNew) { }
}

export class LoadWeatherFail implements Action {
  readonly type = WeatherActionTypes.LoadWeatherFail;
  constructor(public payload: any) { }
}


export type WeatherActions = LoadWeathers | LoadWeatherSuccess | LoadWeatherFail;
