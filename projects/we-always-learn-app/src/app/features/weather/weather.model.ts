export interface Attribute {
  version: string;
}

export interface Header {
  title: string;
  description: string;
  uri: string;
  lastBuildDate: string;
  copyRight: string;
  generator: string;
  status: string;
}

export interface ContactDetail { }

export interface WarningNew {
  IssueNo: string;
  AnnounceDateTime: string;
  TitleThai: string;
  TitleEnglish: string;
  DescriptionThai: string;
  DocumentFile: string;
  WarningTypeThai: string;
  WarningTypeEnglish: string;
  ContactDetail: ContactDetail;
}

export interface WeatherNewsModel {
  '@attributes': Attribute;
  header: Header;
  WarningNews: WarningNew;
}





export interface Observation {
  '@attributes': Attribute;
  dateTime: string;
  meanSeaLevelPressure: string;
  temperature: string;
  Maxtemperature: string;
  DifferentFromMaxtemperature: string;
  Mintemperature: string;
  DifferentFromMintemperature: string;
  relativeHumidity: string;
  windDirection: string;
  windSpeed: string;
  rainfall: string;
}

export interface Station {
  wmoStationNumber: string;
  stationNameThai: string;
  stationNameEnglish: string;
  province: string;
  latitude: string;
  longitude: string;
  observation: Observation;
}

export interface Stations {
  station: Station[];
}

export interface WeatherTodayModel {
  '@attributes': Attribute;
  header: Header;
  stations: Stations;
}
