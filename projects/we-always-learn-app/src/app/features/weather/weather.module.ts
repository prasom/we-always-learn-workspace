import { WeatherEffects } from './weather.effects';

import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherLandingComponent } from './weather-landing/weather-landing.component';
import { StoreModule } from '@ngrx/store';
import { weatherFeatureKey, reducer } from './weather.reducer';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [WeatherLandingComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedModule,
    WeatherRoutingModule,
    StoreModule.forFeature(weatherFeatureKey, reducer),
    EffectsModule.forFeature([
      WeatherEffects
    ])
  ]
})
export class WeatherModule { }
