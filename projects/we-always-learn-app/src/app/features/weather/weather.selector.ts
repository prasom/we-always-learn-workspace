import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State, weatherFeatureKey } from './weather.reducer';


// export const selectFeature = (state: State) => state;
export const selectFeature = createFeatureSelector<State>(weatherFeatureKey);
export const selectNews = createSelector(
  selectFeature,
  (state: State) => state.news
);

export const selectNewsLoading = createSelector(
  selectFeature,
  (state: State) => state.newsLoading
);
