import { selectNewsLoading } from './../weather.selector';
import { Observable } from 'rxjs';
import { LoadWeathers } from './../weather.actions';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../weather.reducer';
import { WarningNew } from '../weather.model';
import { selectNews } from '../weather.selector';

@Component({
  selector: 'app-weather-landing',
  templateUrl: './weather-landing.component.html',
  styleUrls: ['./weather-landing.component.scss']
})
export class WeatherLandingComponent implements OnInit {

  news$: Observable<WarningNew>;
  newsLoading$: Observable<boolean>;
  constructor(public store: Store<State>) { }

  ngOnInit() {
    this.news$ = this.store.pipe(select(selectNews));
    this.newsLoading$ = this.store.pipe(select(selectNewsLoading));
    this.store.dispatch(new LoadWeathers());
  }

}
