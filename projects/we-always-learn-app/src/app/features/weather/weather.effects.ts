
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { concatMap, map, switchMap, catchError, tap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { WeatherActionTypes, WeatherActions, LoadWeatherSuccess, LoadWeatherFail } from './weather.actions';
import { WeatherService } from './weather.service';



@Injectable()
export class WeatherEffects {


  @Effect()
  loadWeathers$ = this.actions$.pipe(
    ofType(WeatherActionTypes.LoadWeathers),
    map(action => action),
    tap(action => console.log(action)),
    switchMap(() => this.weatherService.getWeatherNews().pipe(
      map(res => new LoadWeatherSuccess(res.WarningNews)),
      catchError(err => of(new LoadWeatherFail(err)))
    ))
  );



  constructor(private actions$: Actions<WeatherActions>, private weatherService: WeatherService) { }

}
